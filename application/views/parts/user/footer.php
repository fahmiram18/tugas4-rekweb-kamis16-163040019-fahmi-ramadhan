	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script>
    	$(document).ready(function() {
    		$('.add_cart').click(function() {
    			var harga = $(this).data("harga");
    			var nama = $(this).data("nama");
    			var id = $(this).data("id");
    			var kuantitas = 1;
                var order = 0;
				
				$.ajax({
					url:"<?php echo base_url('shop/tambah') ?>",
					method:"post",
					data:{
						harga:harga, nama:nama, id:id, kuantitas:kuantitas
					},
					success:function(data) {
						alert("Produk telah ditambahkan kedalam keranjang");
                        $('#cart_details').html(data);
					}
				})
    		})
    	});

        $('#cart_details').load("<?php echo base_url('shop/load') ?>");

        $(document).on('click', '.remove_inventory', function(){
            var row_id = $(this).attr("id");
            if (confirm('Yakin hapus dari keranjang?')) {
                $.ajax({
                    url:"<?php echo base_url('shop/hapus') ?>",
                    method:"post",
                    data:{row_id:row_id},
                    success:function(data){
                        alert("Produk telah dihapus");
                        $('#cart_details').html(data);
                    }
                })
            } else {
                return false;
            }
        })

        $(document).on('click', '#clear_cart', function(){
            var row_id = $(this).attr("id");
            if (confirm('Yakin bersihkan keranjang?')) {
                $.ajax({
                    url:"<?php echo base_url('shop/clear') ?>",
                    success:function(data){
                        alert("Keranjang telah dibersihkan!");
                        $('#cart_details').html(data);
                    }
                })
            } else {
                return false;
            }
        });

        function handleClickDetail(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('shop/getDataProduk') ?>",
            data: "id=" + id,
            dataType: "json",
            success: function(data) {
                // var a = JSON.parse(data);
                $('#modal-title').html(data.nama_produk);
                $("#tahun").html('Tahun edar : ' + data.tahun_produk);
                $("#stok").html('Stok : ' + data.stok + ' unit');
                $("#harga").html("Harga : Rp. " + data.harga);
                $("#kategori").html('Kategori : ' + data.kategori_produk);
            }
        });
    };
    </script>
  </body>
</html>