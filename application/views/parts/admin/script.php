<script type="text/javascript">
	// $(document).ready(function() {
	// 	$('#tbl_produk').DataTable({
	// 	    "language": {
	//             "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
	// 		    "sProcessing":   "Sedang memproses...",
	// 		    "sLengthMenu":   "Tampilkan _MENU_ entri",
	// 		    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
	// 		    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
	// 		    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
	// 		    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
	// 		    "sInfoPostFix":  "",
	// 		    "sSearch":       "Cari:",
	// 		    "sUrl":          "",
	// 		    "oPaginate": {
	// 		        "sFirst":    "Pertama",
	// 		        "sPrevious": "Sebelumnya",
	// 		        "sNext":     "Selanjutnya",
	// 		        "sLast":     "Terakhir"
	// 		    }
	//         },
	// 		"processing": true,
	// 		"serverSide": true,
	// 		"iDisplayLength": 5,
 //    		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "Semua"]],
	// 		"order": [],
	// 		"ajax": {
	// 			"url": "<?php echo site_url('produk/get_data_produk')?>",
	// 			"type": "POST"
	// 		},
	// 		columnDefs: [{
	// 			"targets": [ 0, 1 ],
	// 			"orderable": false,
	// 		}],           
	// 	});       
	// });
	var arrayReturn = [];
    $.ajax({
        url: "http://localhost/api-produk/produk",
        async: false,
        dataType: 'json',
        success: function (data) {
			for (var i = 0, len = data.length; i < len; i++) {
				arrayReturn.push([
					i+1, 
					'<img width="50px" src="<?php echo site_url('assets/img/produk/') ?>' + data[i].gambar_produk + '" alt=" ">', 
					data[i].nama_produk, 
					data[i].tahun_produk, 
					data[i].stok, 
					data[i].harga, 
					data[i].kategori_produk, 
					'<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-ubah" onclick="handleClickUbah('+ data[i].id_produk +')"><i class="fa fa-gear"></i></button><a href=" <?php echo base_url("produk/hapus/")?>'+ data[i].id_produk +'"><button type="button" class="btn btn-danger" onclick="return confirm(\'Data yang di hapus tidak dapat dikembalikan. Yakin hapus?\')"><i class="fa fa-trash"></i></button></a>'
					]);
			}
		inittable(arrayReturn);
        }
    });
    function inittable(data) {
    	console.log(data);
		$('#tbl_produk').DataTable({
			"aaData": data,
			 "language": {
	            "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
			    "sProcessing":   "Sedang memproses...",
			    "sLengthMenu":   "Tampilkan _MENU_ entri",
			    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
			    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
			    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
			    "sInfoPostFix":  "",
			    "sSearch":       "Cari:",
			    "sUrl":          "",
			    "oPaginate": {
			        "sFirst":    "Pertama",
			        "sPrevious": "Sebelumnya",
			        "sNext":     "Selanjutnya",
			        "sLast":     "Terakhir"
			    }
	        },
			"processing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "Semua"]],
			"order": [],
			columnDefs: [{
				"targets": [ 1 ],
				"orderable": false,
			}],
		} );
	}
	
	function handleClickUbah(id) {
		$.ajax({
			url: "<?php echo site_url('produk/edit/') ?>",
			type: "POST",
			data: "id=" + id,
			dataType: 'json',
			success: function (data) {
				if (data.status == 200) {
					$("#id_ubah").val(data['produk'][0].id_produk);
	                $("#id_produk").val(data['produk'][0].id_produk);
	                $("#gambar_lama").val(data['produk'][0].gambar_produk);
	                $("#nama_produk").val(data['produk'][0].nama_produk);
	                $("#tahun_produk").val(data['produk'][0].tahun_produk);
	                $("#stok").val(data['produk'][0].stok);
	                $("#harga").val(data['produk'][0].harga);
	                $("#kategori_produk").val(data['produk'][0].kategori_produk);
				}
			},
			error: function (xhr, status, error) {
				alert(status + " : " + error);
			}
		})
    };

    $(document).ready(function() {
    	$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url: "<?php echo site_url('produk/tambah') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#formTambah").serialize(),
				success: function () {
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false
					});
					setTimeout(function () {
						location.reload();
					}, 2000);
				},
				error: function (xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});


	    $("#submitUpdate").click(function (e) {
			e.preventDefault();
			$.ajax({
				// url: "<?php echo site_url('produk/update') ?>",
				type: "POST",
				// data: $("#formUpdate").serialize(),
				dataType: "JSON",
				success: function () {
					$('#modal-ubah').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false
					});
					setTimeout(function () {
						location.reload();
					}, 2000);
				},
				error: function (xhr, status, error) {
					alert(status + " : " + error);
				}
			})
		});
	})  
</script>
</body>
</html>