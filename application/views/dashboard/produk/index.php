    <?php echo $this->session->flashdata('status'); ?>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h1 class="box-title">Tabel Produk</h1>
                </div>
                <div class="box-body">
                    <!-- <button type="button" class="btn btn-success" style="margin-bottom: 10px;" data-toggle="modal" href='#modal-tambah'> <i class="fa fa-plus"></i> Tambah Produk</button> -->
                    <table id="tbl_produk" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Gambar</th>
                            <th>Nama Produk</th>
                            <th>Tahun Produk</th>
                            <th>Stok</th>
                            <th>Harga</th>
                            <th>Kategori Produk</th>
                            <th>Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>

    <!-- Modals -->
    <div class="modal fade" id="modal-ubah">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Ubah Data Produk</h4>
                </div>
                <form id="formUpdate" action="<?php echo base_url('produk/ubah') ?>" method="POST" role="form" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" name="id_ubah" id="id_ubah" value="0">
                    <input type="hidden" class="form-control" name="gambar_lama" id="gambar_lama" value=" ">
                    <div class="modal-body">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="number" name="id_produk" id="id_produk" class="form-control" value="0" disabled>
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                <input type="text" name="nama_produk" id="nama_produk" class="form-control" value=" ">
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <select name="tahun_produk" id="tahun_produk" class="form-control" value="0">
                                    <option value="">---Pilih Tahun---</option>
                                    <?php for ($i = 1990; $i <= 2020; $i++): ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php endfor ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-table"></i></span>
                                <input type="number" name="stok" id="stok" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" name="harga" id="harga" class="form-control" value="0">
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                <select name="kategori_produk" id="kategori_produk" class="form-control" value="0">
                                    <option value="">---Pilih Kategori---</option>
                                    <option value="Motor">Motor</option>
                                    <option value="Mobil">Mobil</option>
                                </select>
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-12">
                            <div class="input-group">
                                <input type="file" name="gambar_baru" id="gambar_baru" value=" ">
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" name="submit" class="btn btn-primary" id="submitUpdate">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div> 