<div class="row">
	<div class="col-md-2"></div>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="box">
			<div class="box-header">
				<h1 class="box-title">Tambah Produk</h1>
			</div>
			<div class="box-body">
				<form id="formTambah" action="<?php echo base_url('produk/tambah') ?>" method="post" role="form" enctype="multipart/form-data">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-font"></i></span>
						    <input type="text" name="nama_produk" class="form-control" placeholder="Nama Produk">
						</div>
						<br>
					</div>
					<div class="col-xs-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<select name="tahun_produk" id="tahun_produk" class="form-control">
								<option value="">---Pilih Tahun---</option>
								<?php for ($i = 1990; $i <= 2020; $i++): ?>
								<option value="<?php echo $i ?>"><?php echo $i ?></option>
								<?php endfor ?>
							</select>
						    <!-- <input type="number" name="tahun_produk" min="1990" max="2099" step="1" class="form-control" placeholder="Tahun Keluaran"> -->
						</div>
					</div>
					<div class="col-xs-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-table"></i></span>
						    <input type="number" name="stok" class="form-control" placeholder="Stok Produk">
						</div>
					</div>
					<div class="col-xs-4">
						<div class="input-group">
							<span class="input-group-addon">Rp.</span>
						    <input type="number" name="harga" class="form-control" placeholder="Harga Produk">
						</div>
						<br>
					</div>
					<div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                <select name="kategori_produk" id="kategori_produk" class="form-control">
                                    <option value="">---Pilih Kategori---</option>
                                    <option value="Motor">Motor</option>
                                    <option value="Mobil">Mobil</option>
                                </select>
                            </div>
                            <br>
                        </div>
					<div class="col-xs-12">
						<div class="input-group">
						    <input type="file" name="gambar_produk" id="gambar_produk">
						</div>
						<br>
					</div>
					<div class="col-xs-12">
						<button type="submit" name="submit" id="submitAdd" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
	<?php echo validation_errors(); ?>
</div>