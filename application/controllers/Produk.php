<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->API = "http://localhost/api-produk";
        $this->load->model('Produk_model');
        if (!$this->session->userdata('username')) {
            redirect('admin/login');
        }
    }
	 
    public function index()
    {
    	$data['judul'] = "Daftar Produk";
        $data['content'] = 'dashboard/produk/index';
        $this->load->view('templates/admin', $data);
    }

    // datatables
    public function get_data_produk()
    {
        $list = $this->Produk_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<img width="50px" src="'.site_url('assets/img/produk/'.$field->gambar_produk).'" alt=" ">';
            $row[] = $field->nama_produk;
            $row[] = $field->tahun_produk;
            $row[] = $field->stok . ' Unit';
            $row[] = 'Rp. '.number_format($field->harga,0,',','.').',-';
            $row[] = $field->kategori_produk;
            $row[] = '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-ubah" onclick="handleClickUbah('.$field->id_produk.')"><i class="fa fa-gear"></i></button>
                <a href="'.base_url('produk/hapus/'.$field->id_produk).'"><button type="button" class="btn btn-danger" onclick="return confirm(\'Data yang di hapus tidak dapat dikembalikan. Yakin hapus?\')"><i class="fa fa-trash"></i></button></a>';
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Produk_model->count_all(),
            "recordsFiltered" => $this->Produk_model->count_filtered(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function edit()
    {
        $id = $this->input->post('id');
        $params = array('id' => $id);
        $data['produk'] = json_decode($this->curl->simple_get($this->API.'/produk/', $params));
        $produk = $data['produk'];
        $json = json_encode(array("status" => 200, "produk" => $produk));
        echo $json;
    }

    public function tambah()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $data['judul'] = "Daftar Produk";
            $data['content'] = 'dashboard/produk/tambah_form';
            $this->load->view('templates/admin', $data);
        } else {
            $config['file_name'] = 'img_'.time();
            $config['upload_path'] = './assets/img/produk/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size'] = 100;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar_produk')){
                $data['judul'] = "Daftar Produk";
                $data['content'] = 'dashboard/produk/tambah_form';
                $this->load->view('templates/admin', $data);
            } else {
                $nama = $this->input->post('nama_produk');
                $tahun = $this->input->post('tahun_produk');
                $stok = $this->input->post('stok');
                $harga = $this->input->post('harga');
                $kategori = $this->input->post('kategori_produk');
                $data = [
                    'gambar' => $this->upload->data('file_name'),
                    'nama' => $nama,
                    'tahun' => $tahun,
                    'stok' => $stok,
                    'harga' => $harga,
                    'kategori' => $kategori
                ];
                
                $status = $this->curl->simple_post($this->API.'/produk/', $data, array(CURLOPT_BUFFERSIZE => 10));
                if ($status) {
                    $this->session->set_flashdata('status', 
                        '<div class="alert alert-success alert-dismissible" style="position:fixed; right:0; z-index:100;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                            Produk berhasil ditambahkan.
                        </div>'
                    );
                } else {
                    $this->session->set_flashdata('status', 
                        '<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            Produk gagal ditambahkan.
                        </div>'
                    );
                }
                redirect('produk');
            }
        }
    }

    public function hapus($id)
    {
        $status = $this->Produk_model->hapus($id);
        if ($status) {
                $this->session->set_flashdata('status', 
                    '<div class="alert alert-success alert-dismissible" style="position:fixed; right:0; z-index:100;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Alert!</h4>
                        Produk berhasil dihapus.
                    </div>'
                );
            } else {
                $this->session->set_flashdata('status', 
                    '<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        Server error. Produk gagal dihapus.
                    </div>'
                );
            }
            redirect('produk');
    }

    public function ubah()
    {
        $config['file_name'] = 'img_'.time();
        $config['upload_path'] = './assets/img/produk/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload', $config);

        $id = $this->input->post('id_ubah');
        $gambar = $this->input->post('gambar_lama');
        $nama = $this->input->post('nama_produk');
        $tahun = $this->input->post('tahun_produk');
        $stok = $this->input->post('stok');
        $harga = $this->input->post('harga');
        $deskripsi = $this->input->post('kategori_produk');

        if ($this->upload->do_upload('gambar_baru')){
            $gambar = $this->upload->data('file_name');
        }

        $produk = [
                    'gambar_produk' => $gambar,
                    'nama_produk' => $nama,
                    'tahun_produk' => $tahun,
                    'stok' => $stok,
                    'harga' => $harga,
                    'kategori_produk' => $deskripsi
                ];

        $status = $this->Produk_model->ubah($id, $produk);
        if ($status) {
            $this->session->set_flashdata('status', 
                '<div class="alert alert-success alert-dismissible" style="position:fixed; right:0; z-index:100;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    Data produk berhasil diubah!
                </div>'
            );
        } else {
            $this->session->set_flashdata('status', 
                '<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    Server error. Produk gagal diubah!
                </div>'
            );
        }
        redirect('produk');
        
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('tahun_produk', 'Tahun keluar', 'required');
        $this->form_validation->set_rules('stok', 'Stok Produk', 'required');
        $this->form_validation->set_rules('harga', 'Harga Produk', 'required');
        $this->form_validation->set_rules('kategori_produk', 'Deskripsi', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>', '</div>');
    }
}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */