<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	public function index()
	{
		$data['judul'] = "Daftar Pesanan";
		$data['content'] = 'dashboard/pesanan/index';
		$this->load->view('templates/admin', $data);
	}

}

/* End of file Pesanan.php */
/* Location: ./application/controllers/Pesanan.php */