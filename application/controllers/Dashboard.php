<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');
		if (!$this->session->userdata('username')) {
			redirect('admin/login');
		}
	}

	public function index()
	{			
		$data = [
			'data_produk' => $this->Dashboard_model->count_data('tbl_produk'),
			'data_order' => 0,
			'data_user' => 0
		];
		$data['judul'] = "Dashboard";;
		$data['content'] = 'dashboard/index';
		$this->load->view('templates/admin', $data);

	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */