<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('shop_model');
	}

	public function index()
	{
		$data['judul'] = 'Store';
		$data['content'] = 'shop/index';
		$data['products'] = $this->shop_model->getAllProduk();
		$data['order'] = $this->count_order();
		$this->load->view('templates/index', $data);
	}

	public function getDataProduk()
	{
		$id = $this->input->post('id');
        $data = $this->shop_model->getDataById($id);
        echo json_encode($data);
	}

	public function keranjang()
	{
		// foreach ($this->cart->contents() as $key => $value) {
			
		// }
		$data['judul'] = 'keranjang';
		$data['content'] = 'shop/keranjang';
		$data['order'] = $this->count_order();
		$this->load->view('templates/index', $data);
	}

	public function count_order()
	{
		$count = 0;
		foreach ($this->cart->contents() as $a) {
			$count++;
		}

		return $count;
	}

	public function tambah()
	{
		$data = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('nama'),
			'qty' => $this->input->post('kuantitas'),
			'price' => $this->input->post('harga')
		);		
		$this->cart->insert($data);
		echo $this->view();
	}

	public function hapus()
	{
		$row_id = $this->input->post('row_id');
		$data = array(
			'rowid' => $row_id,
			'qty' => 0
		);
		$this->cart->update($data);
		echo $this->view();
	}

	public function clear()
	{
		$this->cart->destroy();
		echo $this->view();
	}

	public function load()
	{
		echo $this->view();
	}

	public function view()
	{

		$output = '';
		$output .= '
			<div class="row mt-5">
				<div class="container">
					<div class="col mt-5">
						<button type="button" id="clear_cart" class="btn btn-warning mb-4 text-white">Bersihkan Keranjang</button>
						<table class="table table-hover table-bordered">
							<thead class="thead-dark">
							<tr>
								<th scope="col">Nama Produk</th>
								<th scope="col">Harga</th>
								<th scope="col">Kuantitas</th>
								<th scope="col">Subtotal</th>
								<th scope="col">Opsi</th>
							</tr>
							</thead>
							<tbody>
		';
		$count = 0;
		foreach ($this->cart->contents() as $items) {
			$count++;
			$output .= '
				<tr>
					<td>'. $items["name"] .'</td>
					<td>'. 'Rp. '.number_format($items["price"],0,',','.'). ',-'.'</td>
					<td>'. $items["qty"] .'</td>
					<td>'. 'Rp. '.number_format($items["subtotal"],0,',','.'). ',-'.'</td>
					<td><button type="button" name="remove" class="btn btn-danger remove_inventory" id="'. 	$items["rowid"] .'">Hapus</button></td>
				</tr>
			';
		}
		$output .= '
			<tr>	
				<td colspan="4">Total Harga</td>
				<td>'. 'Rp. '.number_format($this->cart->total(),0,',','.'). ',-'.'</td>
			</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		';

		if ($count == 0) {
			$output = '
			<div class="row mt-5">
				<div class="container">
					<div class="col mt-5">
						<table class="table table-hover table-bordered">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Nama Produk</th>
									<th scope="col">Harga</th>
									<th scope="col">Kuantitas</th>
									<th scope="col">Subtotal</th>
									<th scope="col">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="5"><h3 class="text-center">Keranjang anda kosong!</h3></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>';
		}
		return $output;
	}

}

/* End of file Shop.php */
/* Location: ./application/controllers/Shop.php */